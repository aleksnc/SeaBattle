//GLOBAL//

var INITIAL_BOAT = [4, 3, 3, 2, 2, 2, 1, 1, 1, 1];
var SIZE__STEP = 33;

var listBoat = [];
var allInfoBoat = [];
var allCoord = [];
var nextGen = 0;
var countAttak = 0;


//END GLOBAL//


function Boat() {
    this.vertical = false;
    this.line = 1;
    this.start = [0, 0];
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function compare(a1, a2) {
    return a1.length === a2.length && a1.every(function (v, i) {
        return v === a2[i];
    });
}

function newBoat(lengthBoat) {
    var boat = new Boat();
    var vector = getRandomInt(0, 1);
    if (vector === 1) {
        boat.vertical = true;
    }
    boat.line = lengthBoat;
    var maxCoord = 8 - lengthBoat;

    if (boat.vertical) {
        boat.start[0] = (getRandomInt(0, 8));
        boat.start[1] = (getRandomInt(0, maxCoord))
    } else {
        boat.start[0] = (getRandomInt(0, maxCoord));
        boat.start[1] = (getRandomInt(0, 8));
    }

    coordBoat(boat)
}

function coordBoat(boat) {

    var Xsize = boat.line + 2;
    var Ysize = 3;


    if (boat.vertical) {
        Xsize = 3;
        Ysize = boat.line + 2;
    }
    var myBoat = [];
    var tempAll = [];
    for (var i = 0; i < Xsize; i++) {
        for (var n = 0; n < Ysize; n++) {
            var coord = [];
            coord[0] = boat.start[0] + i;
            coord[1] = boat.start[1] + n;

            if (n > 0 && n < Ysize - 1) {
                if (i > 0 && i < Xsize - 1) {
                    myBoat.push(coord);
                } else {
                    tempAll.push(coord);
                }
            } else {
                tempAll.push(coord);
            }
        }
    }
    var typeCollision = false;
    var ac = 0
    while (ac < allCoord.length) {
        var mb = 0
        while (mb < myBoat.length) {
            if (compare(allCoord[ac], myBoat[mb])) {
                typeCollision = true;
            }
            mb++
        }
        ac++
    }

    if (!typeCollision) {
        var mbl = 0;
        var t = 0;
        while (mbl < myBoat.length) {
            allCoord.push(myBoat[mbl])
            mbl++
        }

        while (t < tempAll.length) {
            allCoord.push(tempAll[t])
            t++
        }
        var temp = []
        temp.push(myBoat);
        temp.push(tempAll);
        temp.push(myBoat.length);
        allInfoBoat.push(temp);
        listBoat.push(myBoat);
        nextGen++;
        if (nextGen < INITIAL_BOAT.length && nextGen < 200) {
            createBoat(nextGen)
        } else {
            allInfoBoat.push(INITIAL_BOAT.length)
            drawBoats();
        }
    }
    else {
        createBoat(nextGen)
    }
}

function drawBoats() {  //for show generator boats (use with css)

    // for (var a = 0; a < allCoord.length; a++) {
    //     $('.parent.boat').clone().removeClass('parent').addClass('boat__space').appendTo('.space__seaField').css({
    //         'left': allCoord[a][0] * SIZE__STEP + 'px',
    //         'top': allCoord[a][1] * SIZE__STEP + 'px'
    //     });
    // }

    // for (var i = 0; i < listBoat.length; i++) {
    //     for (var n = 0; n < listBoat[i].length; n++) {
    //         $('.parent.boat').clone().removeClass('parent').appendTo('.space__seaField').css({
    //             'left': listBoat[i][n][0] * SIZE__STEP + 'px',
    //             'top': listBoat[i][n][1] * SIZE__STEP + 'px'
    //         });
    //     }
    //}
}

function drawBoat(coords, type) {
    // console.log(coord);

    var myClass = 'boat__fall';

    if (type) {
        myClass = 'boat__win';
    }

    var i = 0;
    if (coords.length < 3) {
        point(coords, myClass)
    } else {
        while (i < coords.length) {
            var coord = coords[i];
            point(coord, myClass);
            i++
        }
    }
}

function point(coord, myClass) {
    $('.parent.boat').clone().removeClass('parent').addClass(myClass).appendTo('.space__seaField').css({
        'left': coord[0] * SIZE__STEP + 'px',
        'top': coord[1] * SIZE__STEP + 'px'
    });
}

function actions(coord) {
    var typeCollision = false;
    var ac = 0;
    while (ac < allInfoBoat.length - 1) {
        var mb = 0;
        while (mb < allInfoBoat[ac][0].length) {
            var compareCoord = allInfoBoat[ac][0][mb];

            if (compare(compareCoord, coord)) {
                var lengBoat = allInfoBoat[ac][2];
                lengBoat--;
                allInfoBoat[ac][2] = lengBoat;
                var allCountBoat = allInfoBoat[allInfoBoat.length - 1];
                if (lengBoat <= 0) {
                    drawBoat(allInfoBoat[ac][1], false)
                    allCountBoat--
                    allInfoBoat[allInfoBoat.length - 1] = allCountBoat;
                }

                if (allCountBoat <= 0) {
                    $('.popup').fadeIn()
                    var score =100-countAttak;
                    $('.popup__score span').empty().html(score);
                }
                typeCollision = true;
            }
            mb++
        }
        ac++
    }

    drawBoat(coord, typeCollision)
}

function createBoat(indx) {
    newBoat(INITIAL_BOAT[indx])
}

$(document).ready(function () {
    createBoat(0);

    $('.space__seaField').on('click', '.boat', function (e) {
        return false;
    })

    $('.space__seaField').click(function (e) {
        var offset = $(this).offset();
        var relativeX = (e.pageX - offset.left);
        var relativeY = (e.pageY - offset.top);
        var coord = [];
        coord[0] = Math.floor(relativeX / SIZE__STEP);
        coord[1] = Math.floor(relativeY / SIZE__STEP);
        actions(coord);
        countAttak++
        console.log(countAttak);
    });

    $('.popup__btn').click(function () {
        $('.space__seaField').empty();
        listBoat = [];
        allInfoBoat = [];
        allCoord = [];
        nextGen = 0;
        createBoat(0);
        countAttak = 0;
        $('.popup').fadeOut()
    })
})